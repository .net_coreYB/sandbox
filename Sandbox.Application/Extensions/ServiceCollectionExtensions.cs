﻿using Sandbox.Application.Services;
using Sandbox.Core.Interfaces.Application.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Application.Extensions
{
    public static partial class ServiceCollectionExtensions
    {
        public static void AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IReferentialService, ReferentialService>();

            services.AddRefitClients(configuration);
        }

        public static void AddRefitClients(this IServiceCollection services, IConfiguration configuration)
        {
            // ToDo use Refit for DocuWare
        }
    }
}
