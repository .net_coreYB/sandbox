﻿using Sandbox.Core.DTOs.Referential.Request;
using Sandbox.Core.DTOs.Referential.Response;
using Sandbox.Core.Infrastructure.Data.Repositories;
using Sandbox.Core.Interfaces.Application.Services;
using Sandbox.Core.Specifications;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Application.Services
{
    public class ReferentialService : IReferentialService
    {
        private readonly IReferentialRepository _repository;

        public ReferentialService(IReferentialRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Methode to get lists of Referetials with params
        /// </summary>
        /// <param name="request">Params from Query Request</param>
        /// <returns></returns>
        public async Task<IEnumerable<ReferentialResponse>> List(ReferentialSimpleRequest request)
        {
            var specs = new ReferentialSpecifications(request);

            var allData = _repository.GetAll<ReferentialResponse>(specs);

            return allData;
        }
    }
}
