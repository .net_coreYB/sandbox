﻿using System;
using System.Collections.Generic;

namespace Sandbox.Core.Models;

public partial class Referential
{
    public short Id { get; set; }

    public string Code { get; set; } = null!;

    public string Label { get; set; } = null!;

    public string Destination { get; set; } = null!;

    public string Axis { get; set; } = null!;
}
