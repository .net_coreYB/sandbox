﻿using Sandbox.Core.Models;
using Sandbox.Core.Interfaces.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Infrastructure.Data.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        T GetById(int id, ISpecifications<T> specification = null);
        T GetById(ISpecifications<T> specification);
        Task<T> GetByIdAsync(int id, ISpecifications<T> specification = null);
        Task<T> GetByIdAsync(ISpecifications<T> specification);
        IQueryable<T> GetAll(ISpecifications<T> specification = null);
        IEnumerable<TProject> GetAll<TProject>(ISpecifications<T> specification = null);
        Task<(IEnumerable<TProjectTo> results, int itemsCount)> GetForGridView<TProjectTo>(ISpecifications<T> specification);
        T UpdateAsync(T obj);
        void Delete(T obj);
        Task<T> InsertAsync(T obj);
        Task<int> SaveAsync();
        Task<int> ExecutePS(string sql, params object[] parameters);
    }
}
