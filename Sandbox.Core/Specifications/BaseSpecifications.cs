﻿using Sandbox.Core.Interfaces.Core.Models;
using Sandbox.Core.Interfaces.Specifications;
using Sandbox.Core.Models;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Specifications
{
    public class BaseSpecifications<T> : ISpecifications<T> where T : BaseEntity, IBaseEntity
    {
        #region "Attributes"

        #endregion

        #region "Properties"
        public List<Expression<Func<T, bool>>> Criterias { get; } = new();
        public List<Func<IQueryable<T>, IIncludableQueryable<T, object>>> Includes { get; } = new();
        public Expression<Func<T, object>> OrderBy { get; protected set; }
        public Expression<Func<T, object>> OrderByDescending { get; protected set; }
        public Expression<Func<T, object>> GroupBy { get; protected set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }

        //protected static Expression<Func<T, bool>> NotDeletedOrArchived =
        //    entity => entity.Statut != Enums.BaseEntityStatus.Deleted.Value && entity.Statut != Enums.BaseEntityStatus.Archived.Value;

        #endregion

        #region "Constructors"
        public BaseSpecifications()
        {
        }

        public BaseSpecifications(Expression<Func<T, bool>> criteria)
        {
            Criterias.Add(criteria);
        }
        #endregion

        #region "Methods"
        protected void AddInclude(Func<IQueryable<T>, IIncludableQueryable<T, object>> includeExpression)
        {
            Includes.Add(includeExpression);
        }
        protected void AddOrderBy(Expression<Func<T, object>> orderByExpression)
        {
            OrderBy = orderByExpression;
        }
        protected void AddOrderByDescending(Expression<Func<T, object>> orderByDescExpression)
        {
            OrderByDescending = orderByDescExpression;
        }
        protected void ApplyOrderBy(Expression<Func<T, object>> orderByExpression)
        {
            OrderBy = orderByExpression;
        }

        #endregion
    }
}
