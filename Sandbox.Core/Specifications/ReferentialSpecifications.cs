﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Specifications
{
    public class ReferentialSpecifications : BaseSpecifications<Sandbox.Core.Models.Referential>
    {

        public ReferentialSpecifications(Sandbox.Core.DTOs.Referential.Request.ReferentialSimpleRequest request)
        {

        }

        public ReferentialSpecifications(int idRef)
        {
            Criterias.Add(c => c.Id == idRef);
        }
    }
}
