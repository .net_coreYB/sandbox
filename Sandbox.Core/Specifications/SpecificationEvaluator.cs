﻿using Sandbox.Core.Interfaces.Specifications;
using Sandbox.Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Specifications
{
    public class SpecificationEvaluator<TEntity> where TEntity : BaseEntity
    {
        public static IQueryable<TEntity> GetQuery(IQueryable<TEntity> query, ISpecifications<TEntity> specifications)
        {
            // Do not apply anything if specifications is null
            if (specifications == null)
                return query;

            // Modify the IQueryable
            // Apply filter conditions
            foreach (var criteria in specifications.Criterias)
            {
                query = query.Where(criteria);
            }

            // Includes
            query = specifications.Includes
                                .Aggregate(query, (current, include) => include(current));

            // Apply ordering
            if (specifications.OrderBy != null)
            {
                query = query.OrderBy(specifications.OrderBy);
            }
            else if (specifications.OrderByDescending != null)
            {
                query = query.OrderByDescending(specifications.OrderByDescending);
            }

            // Apply GroupBy
            if (specifications.GroupBy != null)
            {
                query = query.GroupBy(specifications.GroupBy).SelectMany(x => x);
            }



            // Doing this so we don't end up with one of the IQueryable subtypes like IIncludableQueryable
            return query.AsQueryable();
        }

        public static async Task<TEntity> GetSingleAsync(IQueryable<TEntity> query, ISpecifications<TEntity> specifications)
        {
            // Includes
            query = specifications.Includes
                                .Aggregate(query, (current, include) => include(current));

            foreach (var criteria in specifications.Criterias)
            {
                query = query.Where(criteria);
            }
            // Modify the IQueryable
            // Apply filter conditions
            return await query.FirstOrDefaultAsync();
        }

        public static TEntity GetSingle(IQueryable<TEntity> query, ISpecifications<TEntity> specifications)
        {
            query = specifications.Includes
                                .Aggregate(query, (current, include) => include(current));
            foreach (var criteria in specifications.Criterias)
            {
                query = query.Where(criteria);
            }
            return query.FirstOrDefault();
        }

        public static IQueryable<TEntity> AddPagination(IQueryable<TEntity> query, ISpecifications<TEntity> specifications)
        {
            if (specifications == null)
                return query;

            if (specifications.Skip.HasValue)
                query = query.Skip(specifications.Skip.Value);
            if (specifications.Take.HasValue)
                query = query.Take(specifications.Take.Value);

            return query;
        }
    }
}
