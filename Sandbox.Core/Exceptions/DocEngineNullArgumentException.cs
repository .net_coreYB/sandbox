﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Exceptions
{
    [Serializable]
    public class SandboxNullArgumentException : SandboxBaseException
    {
        public SandboxNullArgumentException() { }
        public SandboxNullArgumentException(string message) : base(message) { }
        public SandboxNullArgumentException(string message, Exception inner) : base(message, inner) { }
        protected SandboxNullArgumentException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
