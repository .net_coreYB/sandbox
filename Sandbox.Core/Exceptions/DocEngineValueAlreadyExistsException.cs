﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Exceptions
{
    [Serializable]
    public class SandboxValueAlreadyExistsException : SandboxBaseException
    {
        public SandboxValueAlreadyExistsException() { }
        public SandboxValueAlreadyExistsException(string message) : base(message) { }
        public SandboxValueAlreadyExistsException(string message, Exception inner) : base(message, inner) { }
        protected SandboxValueAlreadyExistsException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
