﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Exceptions
{
    [Serializable]
    public class SandboxBaseException : Exception
    {
        public SandboxBaseException() { }
        public SandboxBaseException(string message) : base(message) { }
        public SandboxBaseException(string message, Exception inner) : base(message, inner) { }
        protected SandboxBaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
