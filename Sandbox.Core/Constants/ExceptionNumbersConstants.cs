﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Constants
{
    public static class ExceptionNumbersConstants
    {
        public static int UniqueConstraintExceptionNumber = 2627;
        public static int UniqueIndexExceptionNumber = 2601;
    }
}
