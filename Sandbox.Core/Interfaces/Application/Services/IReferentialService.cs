﻿
using Sandbox.Core.DTOs.Referential.Request;
using Sandbox.Core.DTOs.Referential.Response;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Sandbox.Core.Interfaces.Application.Services
{
    public interface IReferentialService
    {
        Task<IEnumerable<ReferentialResponse>> List(ReferentialSimpleRequest request);
        
    }
}
