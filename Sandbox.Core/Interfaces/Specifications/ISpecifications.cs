﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Interfaces.Specifications
{
    public interface ISpecifications<T>
    {
        List<Expression<Func<T, bool>>> Criterias { get; }
        List<Func<IQueryable<T>, IIncludableQueryable<T, object>>> Includes { get; }
        Expression<Func<T, object>> OrderBy { get; }
        Expression<Func<T, object>> OrderByDescending { get; }
        Expression<Func<T, object>> GroupBy { get; }

        int? Skip { get; set; }
        int? Take { get; set; }
    }
}
