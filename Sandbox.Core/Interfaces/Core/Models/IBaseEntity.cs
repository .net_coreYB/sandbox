﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Interfaces.Core.Models
{
    public interface IBaseEntity
    {
        short Id { get; set; }
    }
}
