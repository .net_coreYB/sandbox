﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.DTOs.Referential.Response
{
    public class ReferentialResponse
    {
        public short Id { get; set; }

        public string Code { get; set; } = null!;

        public string Label { get; set; } = null!;

        public string Destination { get; set; } = null!;

        public string Axis { get; set; } = null!;
    }
}
