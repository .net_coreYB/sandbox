﻿using AutoMapper;
using Sandbox.Core.Interfaces.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Sandbox.Core.Profiles
{
    public class BaseProfile : Profile
    {
        public BaseProfile()
        {
            CreateMap<IBaseEntity, int>().ConstructUsing(source => source.Id);
        }
    }
}
