﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Core.Profiles
{
    public class ReferentialProfile : Profile
    {
        public ReferentialProfile()
        {
            // Response
            CreateMap<Sandbox.Core.Models.Referential, Sandbox.Core.DTOs.Referential.Response.ReferentialResponse>();

        }
    }
}
