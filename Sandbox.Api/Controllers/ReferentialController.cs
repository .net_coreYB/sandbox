using Sandbox.Core.DTOs.Referential.Response;
using Microsoft.AspNetCore.Mvc;
using Sandbox.Core.Interfaces.Application.Services;
using Azure.Core;
using Sandbox.Api.Controllers.Common;
using AutoMapper;

namespace Sandbox.Api.Controllers
{
    public class ReferentialController : BaseController
    {
        private readonly IReferentialService _service;

        public ReferentialController(IReferentialService service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpGet(Name = "list")]
        public async Task<IActionResult> Get()
        {
            var result = await _service.List(new Core.DTOs.Referential.Request.ReferentialSimpleRequest());

            return Ok(result);
        }
    }
}