﻿using AutoMapper;
using Sandbox.Core.Infrastructure.Data.Repositories;
using Sandbox.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Infrastructure.Data.Repositories
{
    public class ReferentialRepository : GenericRepository<Referential>, IReferentialRepository
    {
        public ReferentialRepository(DocuSignDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
