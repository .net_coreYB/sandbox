﻿using AutoMapper;
using Sandbox.Core.Constants;
using Sandbox.Core.Exceptions;
using Sandbox.Core.Infrastructure.Data.Repositories;
using Sandbox.Core.Interfaces.Specifications;
using Sandbox.Core.Models;
using Sandbox.Core.Specifications;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Infrastructure.Data.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : BaseEntity
    {

        #region "Attributes"
        private readonly DocuSignDbContext context;
        private readonly IMapper mapper;
        private DbSet<T> entities;
        #endregion

        #region "Properties"

        #endregion

        #region "Constructors"
        public GenericRepository(DocuSignDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
            entities = context.Set<T>();
        }
        #endregion

        #region "Methods"
        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new SandboxNullArgumentException(nameof(entity));
            }
            entities.Remove(entity);
        }

        public async Task<int> ExecutePS(string sql, params object[] parameters)
        {
            return await context.Database.ExecuteSqlRawAsync(sql, parameters);
        }

        /// <summary>
        /// Executes the query.
        /// TODO: Discuss the validity of returning an IQueryable here
        /// </summary>
        /// <param name="specification">The query descriptor</param>
        /// <returns></returns>
        public IQueryable<T> GetAll(ISpecifications<T> specification = null)
        {
            if (specification == null)
                return entities;
            else
            {
                return SpecificationEvaluator<T>.GetQuery(entities, specification);
            }
        }

        /// <summary>
        /// Executes the query and Project (Map) the result to the requested DTO/Model type
        /// </summary>
        /// <typeparam name="TProjectTo">The DTO/Model type to project the results to</typeparam>
        /// <param name="specification">The query descriptor</param>
        /// <returns></returns>
        public IEnumerable<TProjectTo> GetAll<TProjectTo>(ISpecifications<T> specification = null)
        {
            IEnumerable<TProjectTo> results;
            IQueryable<T> query;

            if (specification == null)
                query = entities;
            else
                query = SpecificationEvaluator<T>.GetQuery(entities, specification);

            results = mapper.Map<IEnumerable<TProjectTo>>(query);
            return results.AsEnumerable();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id" example="1"></param>
        /// <param name="specification"></param>
        /// <returns></returns>
        /// <exception cref="PershomeNotFoundException"></exception>
        public T GetById(int id, ISpecifications<T> specification = null)
        {
            T result;
            if (specification == null)
                result = entities.Find(id);
            else
                result = SpecificationEvaluator<T>.GetSingle(entities, specification);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="specification"></param>
        /// <returns></returns>
        /// <exception cref="PershomeNotFoundException"></exception>
        public T GetById(ISpecifications<T> specification)
        {
            var result = SpecificationEvaluator<T>.GetSingle(entities, specification);

            return result;
        }

        public async Task<T> GetByIdAsync(int id, ISpecifications<T> specification = null)
        {
            if (specification == null)
                return entities.Find(id);
            return await GetByIdAsync(specification);
        }

        public async Task<T> GetByIdAsync(ISpecifications<T> specification)
        {
            return await SpecificationEvaluator<T>.GetSingleAsync(entities, specification);
        }

        public async Task<(IEnumerable<TProjectTo> results, int itemsCount)> GetForGridView<TProjectTo>(ISpecifications<T> specification)
        {
            var query = SpecificationEvaluator<T>.GetQuery(entities, specification);

            var itemsCount = await query.CountAsync();

            query = SpecificationEvaluator<T>.AddPagination(query, specification);
            var results = mapper.Map<IEnumerable<TProjectTo>>(query);

            return (results.AsEnumerable(), itemsCount);
        }

        public async Task<T> InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new SandboxNullArgumentException(nameof(entity));
            }
            var result = await entities.AddAsync(entity);
            return result.Entity;
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                return await context.SaveChangesAsync();
            }
            // 2627 is unique constraint (includes primary key), 2601 is unique index
            catch (DbUpdateException ex) when (ex.InnerException is SqlException sqlException && (sqlException.Number == ExceptionNumbersConstants.UniqueConstraintExceptionNumber
                                                                                                  || sqlException.Number == ExceptionNumbersConstants.UniqueIndexExceptionNumber))
            {
                throw new SandboxValueAlreadyExistsException($"This {typeof(T).Name} already exists", ex);
            }
        }

        public T UpdateAsync(T obj)
        {
            entities.Attach(obj);
            context.Entry(obj).State = EntityState.Modified;
            return obj;
        }

        #endregion
    }
}
