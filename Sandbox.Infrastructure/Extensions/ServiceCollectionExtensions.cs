﻿using Sandbox.Infrastructure.Extensions.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Infrastructure.Extensions
{
    public static partial class ServiceCollectionExtensions
    {


        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddRepositories(configuration);
            // Add Other Services as "Azure AD", "Mail Jet", "DocuSign" ...
        }
    }
}
